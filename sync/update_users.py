"""This module syncs users from Talentpact ATS DB to MongoDB"""
import socket
import traceback
import pytz
from datetime import datetime
from tzlocal import get_localzone
import pymongo as pymongo


from configs import config as configuration
from helpers import email_functions
from helpers.db_helpers import connect_to_sql_server, fetch_org_config_by_id, fetch_all_user_alt_ids,fetch_org_config

timeZone = str(get_localzone())

QUERY = "select hre.EmployeeID, hre.OfficialEmailAddress, hre.UserId, su.FirstName, su.MiddleName, su.LastName \
         from dbo.HrEmployee as hre \
         inner join dbo.SysUser as su on su.UserID=hre.UserID \
         where hre.OrganizationId=%s and hre.EmploymentStatusID in %s \
         and hre.OfficialEmailAddress is not NULL"

#QUERY = "select hre.EmployeeID, hre.OfficialEmailAddress, hre.UserId, su.FirstName, su.MiddleName, su.LastName," \
#             " wrksite.WorkSiteHierarchy from TalentPact.dbo.HrEmployee as hre  " \
#             " inner join TalentPact.dbo.SysUser as su   on su.UserID=hre.UserID" \
#             "  inner join TalentPact.dbo.HrWorkSite as wrksite on hre.WorkSiteID=wrksite.WorkSiteID" \
#             "  where hre.OrganizationId=%s and hre.EmploymentStatusID in %s " \
#             " and hre.OfficialEmailAddress is not NULL and  wrksite.WorkSiteHierarchy =%s"


def local_logger(old_log_string, new_log_string):
    """

    A local logger for collating logs and sending as email.

    :param old_log_string: The log string already saved so far
    :param new_log_string: The new string to append
    :return: Updated log string
    """
    log_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return old_log_string + u"<br/>" + log_time + u" | " + new_log_string


def check_emails(user, disallowed_domains_list):
    try:
        user_email_split = user["email"].split('@')
        if len(user_email_split) == 2:
            user_email_domain = user_email_split[1]
            if any(user_email_domain == disallowed_domain for disallowed_domain in disallowed_domains_list):
                user["reason"] = "invalid email domain"
        else:
            user["reason"] = "invalid email"
        return user
    except Exception:
        user["reason"] = "invalid email"
        return user


def update_user_data(conf, altId, updateFields):
    """
        Fetches all Users (only alt ids) from Mongo
        :param conf: Configuration details
        :param alt_id: User Alt Id
        :param timezone: User Timezone
        :return:
        """
    result = None
    conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
    database = conn[conf.MONGO_DB_ALT_JINIE]
    coll = database[conf.MONGO_COLL_USERS]
    print coll
    result = coll.update_one({"alt_id": altId}, {'$set': updateFields})
    print result
    return result


def update_sync(config, org_id):
    email_log = ""
    email_log = local_logger(email_log, "Now updating orgid: " + str(org_id))
    try:
        config_in_mongo_cursor = fetch_org_config_by_id(org_id,config)
        for configRecord in config_in_mongo_cursor:
            status_list = configRecord["employment_status_ids"]
            #disallowed_domains = configRecord["disallowed_domains"]
            defaultTimezone = configRecord.get("timezone") if configRecord.get("timezone") else timeZone
            sql_con = connect_to_sql_server(config)
            sql_con.execute(QUERY, (org_id, status_list))
            users_to_sync = sql_con.fetchall()
            users_in_sql = set([x[2] for x in users_to_sync])
            count = 0
            email_log = local_logger(email_log, "total sql users count " + str(len(users_in_sql)))
            for user in users_in_sql:
                alt_id = user
                count = count+1
                update_user_data(config, int(alt_id), {"timezone":defaultTimezone})
            email_log = local_logger(email_log, "Total Updated Users Count " + str(count))
    except Exception:
        email_log = local_logger(email_log, traceback.format_exc())
    print email_log
    print email_functions.send_mail("altml@alt.peoplestrong.com", "Alt Jinie Notification System", [{
        'recipient_email': 'gorla.edukondalu@peoplestrong.com',
        'subject': 'Activity Log  | Jinie Notifications User Update timezone | ' + socket.gethostname(),
        'body': 'Hi, Activity log is below: <hr/>' + email_log + "<hr/><br/>Best,<br/>Jinie | I'm the nu cool!"
    }])

def update_officail_email(config, org_id):
    email_log = ""
    email_log = local_logger(email_log, "Now updating orgid: " + str(org_id))
    try:
        config_in_mongo_cursor = fetch_org_config_by_id(org_id,config)
        for configRecord in config_in_mongo_cursor:
            status_list = configRecord["employment_status_ids"]
            #disallowed_domains = configRecord["disallowed_domains"]
            defaultTimezone = configRecord.get("timezone") if configRecord.get("timezone") else timeZone
            sql_con = connect_to_sql_server(config)
            sql_con.execute(QUERY, (org_id, status_list))
            users_to_sync = sql_con.fetchall()
            count = 0
            email_log = local_logger(email_log, "total sql users count " + str(len(users_to_sync)))
            for user in users_to_sync:
                alt_id = user[2]
                email = str(user[1].strip()).lower()
                count = count+1
                update_user_data(config, int(alt_id), {"official_email":email})
            email_log = local_logger(email_log, "Total Updated Users Count " + str(count))
    except Exception:
        email_log = local_logger(email_log, traceback.format_exc())
    print email_log
    print email_functions.send_mail("altml@alt.peoplestrong.com", "Alt Jinie Notification System", [{
        'recipient_email': 'gorla.edukondalu@peoplestrong.com',
        'subject': 'Activity Log  | Jinie Notifications User Update timezone | ' + socket.gethostname(),
        'body': 'Hi, Activity log is below: <hr/>' + email_log + "<hr/><br/>Best,<br/>Jinie | I'm the nu cool!"
    }])


def convert_datetime_timezone(dt, source_tz, dest_tz):
    tz_src = pytz.timezone(source_tz)
    tz_dest = pytz.timezone(dest_tz)
    dt = dt.replace(tzinfo=None)
    dt = tz_src.localize(dt)
    dt = dt.astimezone(tz_dest)
    return dt.replace(tzinfo=None)


if __name__ == "__main__":
    #update_sync(configuration, 19)
    update_officail_email(configuration, 9)
