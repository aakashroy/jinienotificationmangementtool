"""This module sends the DAAS subscription notification to users"""
import csv
import requests
import json
import base64


def get_subsction_notification_body(user_id):
    daas_url ="https://outlook.zippi.co/?user_id="+str(user_id)
    #encode_content = "__base64__"+base64.b64encode('{"payload":{},"context":"MOTD_LINK_REMINDER"}')
    final_layout = {
            "input": {
            "messageType": "",
            "action": None,
            "usersList": [
                {
                "surveyID": None,
                "userId": user_id,
                "tenantId": None,
                "organizationId": None,
                "action": "",
                "messageType": None,
                "text": [
                    {
                    "design": [
                        {
                        "buttons": [
                            {
                            "action": 3,
                            "label": "Link Outlook",
                            "url": daas_url
                            },
                            {
                            "label": "Later",
                            "messageText": "Later",
                            "action": 2,
                             "content": "__base64__eyJwYXlsb2FkIjp7fSwiY29udGV4dCI6Ik1PVERfTElOS19SRU1JTkRFUiJ9"
                            }
                    ],
                    "label": "Time is precious. Get a quick summary of all your emails and meetings for the day from Jinie. Link your Outlook account now.",
                    "type": "QUESTION",
                    "subType": "",
                    "displayDataType": "Info",
                    "selectionType": "none",
                    "questionID": None,
                    "tableTitle": None,
                    "tableSubTitle": None,
                    "sequence": None,
                    "dataArray": None,
                    "genericArray": None,
                    "displaySeq": None,
                    "cellTrackerID": None,
                    "action": "",
                    "messageId": None,
                    "selectionOptions": None,
                    "selectedValue": [],
                    "flow": None
                    }
                ]
            }
            ]}]}}
    return final_layout
def send_subscription_notification(csv_path):
    with open(csv_path, 'r') as file:
        reader = csv.reader(file)
        for row in reader:
            print(str(row[1]))
            notification_body = get_subsction_notification_body((row[1]))
            response = requests.post(url="https://messenger.peoplestrong.com/custom/jinieNotificationRequest",
                                    headers={"Content-Type": "application/json",
                                              "Authorization": "JinieNotificationITGGN575"},
                                     data=json.dumps(notification_body, separators=(',', ':')),
                                     timeout=30)
            print(notification_body)

if __name__ == "__main__":
    csv_path = "/home/gorla/motd_pending_subscriptions.csv"
    send_subscription_notification(csv_path)
