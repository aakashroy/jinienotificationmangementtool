"""This module returns the motd subscribers users"""
import csv
from datetime import datetime
from configs import config as configuration
from helpers.db_helpers import fetch_subscription_pending_users
import requests
import json

def local_logger(old_log_string, new_log_string):
    """

    A local logger for collating logs and sending as email.

    :param old_log_string: The log string already saved so far
    :param new_log_string: The new string to append
    :return: Updated log string
    """
    log_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return old_log_string + u"<br/>" + log_time + u" | " + new_log_string

def get_subscription_pending_users(config, org_id,csv_path):
    subscription_pending_users = fetch_subscription_pending_users(org_id,   config)
    with open(csv_path, mode='w') as csv_file:
        fieldnames = ['email', 'alt_id', 'timezone']
        writer = csv.writer(csv_file)
        writer.writerow(fieldnames)
        for row in subscription_pending_users:
            writer.writerow([row['email'], row['alt_id'], row['timezone']])

if __name__ == "__main__":
    csv_path = "/home/gorla/motd_pending_subscriptions.csv"
    get_subscription_pending_users(configuration,19,csv_path)

