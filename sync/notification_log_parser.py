"""This module returns the motd subscribers users"""
import csv
import json
import emails
from jinja2 import Template, Environment, FileSystemLoader


from datetime import datetime
from configs import config as configuration
from helpers.db_helpers import fetch_subscription_pending_users

def local_logger(old_log_string, new_log_string):
    """

    A local logger for collating logs and sending as email.

    :param old_log_string: The log string already saved so far
    :param new_log_string: The new string to append
    :return: Updated log string
    """
    log_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return old_log_string + u"<br/>" + log_time + u" | " + new_log_string


def get_time_val(line):
    time_line = line.split('|')
    return time_line[2]

status_code_dict = {"2XX": 0,"3XX": 0,"4XX": 0,"5XX": 0}
tableData = {'MOTD':{"2XX": 0,"3XX": 0,"4XX": 0,"5XX": 0},
             'UPCOMING_MEETING':{"2XX": 0,"3XX": 0,"4XX": 0,"5XX": 0},
             'OTHERS':{"2XX": 0,"3XX": 0,"4XX": 0,"5XX": 0}
             }
def get_table_record(entry):
    n_type = entry["type"]
    if  n_type != "MOTD" and n_type != "UPCOMING_MEETING":
        n_type = "OTHERS"
    print n_type
    print "Type "+ str(tableData)
    if n_type in tableData:
        statusCode = entry['statusCode']
        if statusCode.startswith('2'):
            count_val = tableData[n_type]["2XX"]+1
            tableData[n_type]["2XX"] = count_val
        elif statusCode.startswith('3'):
            count_val = tableData[n_type]["3XX"] + 1
            tableData[n_type]["3XX"] = count_val
        elif statusCode.startswith('4'):
            count_val = tableData[n_type]["4XX"] + 1
            tableData[n_type]["4XX"] - count_val
        elif statusCode.startswith('5'):
            count_val = tableData[n_type]["5XX"] + 1
            tableData[n_type]["5XX"] = count_val

def get_motd_notifications_log(filePath,csvPath, wordsList,emailRecieptList):
    finalReport = []
    startCheck = False
    readJson = True
    notificationJsonObj=""
    csvElement = dict()
    isToday = False
    tableInfo = []
    with open(filePath) as logFile:
        logFile = logFile.readlines()

    for line in logFile:
        if wordsList[0] in line:
            startCheck=True
            readJson = False
            csvElement = dict()
            csvElement['time'] = get_time_val(line)
            timeVal = get_time_val(line).strip()
            logDateTime = datetime.strptime(timeVal, "%Y-%m-%d %H:%M:%S")
            #print "System Date " + str(datetime.today().date())
            #print "LogTime "+ str(logDateTime.date())
            if logDateTime.date() == datetime.today().date():
                #print "dates are equal"
                isToday = True
            else:
                isToday = False
        if wordsList[1] in line and startCheck:
            readJson = True
            startCheck = False
            notificationJsonObj =""
        if readJson and wordsList[1] not in line and wordsList[0] not in line and wordsList[2] not in line:
            notificationJsonObj = notificationJsonObj+line
        if wordsList[2] in line:
            readJson = False
            jsonObj = json.loads(notificationJsonObj)
            #print notificationJsonObj
            notificationJsonObj = ""
            userId = jsonObj['input']['usersList'][0]['userId']
            msgType = jsonObj['input']['usersList'][0]['text'][0]['design'][0]['type']
            print msgType
            if msgType =='MOTD':
                csvElement['type'] = 'MOTD'
            elif "you have an upcoming meeting" in jsonObj['input']['usersList'][0]['text'][0]['design'][0]['label']:
                csvElement['type'] = 'UPCOMING_MEETING'
            else :
                csvElement['type'] = jsonObj['input']['usersList'][0]['text'][0]['design'][0]['label']
            csvElement['userId'] = userId
            statusCode = line.split('[')[1].split(']')[0]
            csvElement['statusCode'] = statusCode
            if isToday == True:
                finalReport.append(csvElement)
    total_Notifications = 0
    #finalReport = []
    with open(csvPath, mode='w') as csv_file:
        fieldnames = ['userId', 'statusCode', 'type', 'time']
        writer = csv.writer(csv_file)
        writer.writerow(fieldnames)
        for row in finalReport:
            total_Notifications = total_Notifications+1
            get_table_record(row)
            writer.writerow([row['userId'], row['statusCode'], row['type'], row['time']])
    print tableData
    finalReport = [{"type": "MOTD", "2XX": tableData["MOTD"]["2XX"], "3XX": tableData["MOTD"]["3XX"],
                    "4XX": tableData["MOTD"]["4XX"], "5XX": tableData["MOTD"]["5XX"]},
                   {"type": "UPCOMING_MEETING", "2XX": tableData["UPCOMING_MEETING"]["2XX"],
                    "3XX": tableData["UPCOMING_MEETING"]["3XX"], "4XX": tableData["UPCOMING_MEETING"]["4XX"],
                    "5XX": tableData["UPCOMING_MEETING"]["5XX"]},
                   {"type": "OTHERS", "2XX": tableData["OTHERS"]["2XX"], "3XX": tableData["OTHERS"]["3XX"],
                    "4XX": tableData["OTHERS"]["4XX"], "5XX": tableData["OTHERS"]["5XX"]}]

    #message = emails.html(subject='Notification Report',
    #                      text='MOTD notifications log report',
    #                      mail_from=('Gorla Edukondalu', 'gorla.edukondalu@peoplestrong.com'))

    file_loader = FileSystemLoader(searchpath="./")
    env = Environment(loader=file_loader)
    template = env.get_template('./notification_report_template.html')
    renderedTemplate = template.render(finalReport=finalReport, name="Hi")
    message = emails.html(subject='Notification Report {}'.format(datetime.now().strftime("%Y-%m-%d %I:%M:%p")),
                          html=renderedTemplate,
                          mail_from=('Zippi Messenger', 'zippi@peoplestrong.com'))

    message.attach(data=open(csvPath, 'rb'), filename='motd_report.csv')
    res = message.send(to=emailRecieptList,
                 smtp={'host': 'email-1.peoplestrong.com',
                       'port':  587,
                       'user':  'altml@alt.peoplestrong.com',
                       'password':  'AltData@456',
                       'tls':True})
    print res



if __name__ == "__main__":

    wordsList = ["Sending Jinie Notification to",
                "About to send notification:",
                "Received response:"]
    csvPath = '/tmp/motd_logger_parser.csv'
    #emailRecieptList = [('Gorla Edukondalu', 'gorla.edukondalu@peoplestrong.com'),('Ruchi Rana', 'ruchi.rana@peoplestrong.com')]
    #loggerPath = "/home/gorla/evyh.log"
    emailRecieptList = [('Gorla Edukondalu', 'gorla.edukondalu@peoplestrong.com'),('Aakash Roy', 'aakash.roy@peoplestrong.com'),('Siddhartha Tomar', 'siddhartha.tomar@peoplestrong.com')]
    loggerPath = "/home/aakash.roy@psnet.com/.forever/evyh.log"

    get_motd_notifications_log(loggerPath,csvPath, wordsList,emailRecieptList)
