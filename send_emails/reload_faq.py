import requests
import json
import smtplib
import traceback

EMAIL_TEMPLATE = """From: %s <%s>
To: %s
MIME-Version: 1.0
Content-type: text/html
Subject: %s

%s
"""


def send_mail(sender_email, friendly_sender_name, messages, options=None):
    smtp_server = None
    response = {'status': False, 'message': ''}
    if not sender_email and not isinstance(sender_email, basestring):
        response['message'] = "Parameter sender_email should be string or unicode."
    elif not friendly_sender_name and not isinstance(friendly_sender_name, basestring):
        response['message'] = "Parameter friendly_sender_name should be string or unicode."
    elif not isinstance(messages, list):
        response['message'] = "Parameter messages should be a list."
    elif sender_email == "altml@alt.peoplestrong.com":
        try:
            smtp_server = smtplib.SMTP(host="email-1.peoplestrong.com", port=587)
            smtp_server.starttls()
            smtp_server.login(sender_email, "AltData@456")
            results = []
            success = True
            for message in messages:
                smtp_message = EMAIL_TEMPLATE % (
                friendly_sender_name, sender_email, message['recipient_email'], message['subject'], message['body'])
                response = smtp_server.sendmail(from_addr=sender_email, to_addrs=message['recipient_email'],
                                                msg=smtp_message)
                # response is empty if everything went well
                if response:
                    success = False
                results.append(response)

            if success:
                response = {'status': True, 'message': ''}
            else:
                response['message'] = str(results)

        except Exception:
            response['message'] = traceback.format_exc()
    else:
        response['message'] = "Sender email is incorrect. You should know the sender email to use this function!"

    try:
        if smtp_server:
            smtp_server.quit()
    except Exception:
        pass

    return response


def test_send_mail():
    print("sending Email")
    print send_mail(sender_email="altml@alt.peoplestrong.com",
                    friendly_sender_name="Amoeba",
                    messages=[{
                        "recipient_email": "gorla.edukondalu@peoplestrong.com",
                        "subject": "Error Faq happend",
                        "body": "Please Check start a meeting FAQ is not working"
                    }
                    ])



def test_faq_status():
    notification_body = {
                "text":"Start a Microsoft teams meeting",
                "reply":False,
                "jabberId":"1171793@s2demo.peoplestrong.com"
                }
    response = requests.post(url="https://messenger.peoplestrong.com/XmppJinie/xmppJinieRest",
                                    headers={"Content-Type": "application/json",
                                              "Authorization": "JinieNotificationITGGN575"},
                                     json=notification_body
                                     )
    if response.status_code ==200:
        res = json.loads(response.text)["genericRequest"]["text"][0]["design"]["label"]
        if "OK" in res:
            print("Success")
        if "Oops! Looks like you haven't linked your account with Office 365 yet" in res:
            print("Success")
        else:
            print(res)
            test_send_mail()
    else:
        print(response)


if __name__ == "__main__":
    test_faq_status()
