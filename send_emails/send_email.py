"""This module returns the motd subscribers users"""
import csv
import time
import json
import traceback
import emails
from jinja2 import Template, Environment, FileSystemLoader


from datetime import datetime


def send_email_to_user(html_path,bcc_users, row, weblink=None):
    try:
        if(row[2]!=''):
            file_loader = FileSystemLoader(searchpath="./")
            env = Environment(loader=file_loader)
            template = env.get_template(html_path)
            email = row[2]
            print(email)
            username = row[0].decode("utf8")
            domain = row[1].decode("utf8")
            login = str(row[2]).decode("utf8")
            password = str(row[3]).decode("utf8")
            if weblink is None:
                renderedTemplate = template.render(username=username, domain=domain, loginId=login, password=password)
            else:
                renderedTemplate = template.render(username=username, domain=domain, loginId=login, password=password,weblink=weblink)
            message = emails.html(subject='Welcome to Zippi',
                                  bcc= bcc_users,
                                  html=renderedTemplate,
                                  mail_from=('Zippi Messenger', 'support@zippi.co'))
            res = message.send(to=email,
                               smtp={'host': 'email-1.peoplestrong.com',
                                     'port': 587,
                                     'user': 'altml@alt.peoplestrong.com',
                                     'password': 'AltData@456',
                                     'tls': True})
            if not res.status_code:
                print row[0]+","+row[1]+","+row[2]+","+row[3]
            else:
                print res
    except Exception:
        print row[0]+","+row[1]+","+row[2]+","+row[3]
        print traceback.format_exc()


def send_html_by_csv(html_path, bcc_users, csv_path):
    with open(csv_path, 'r') as file:
        reader = csv.reader(file)
        next(reader)
        for row in reader:
            send_email_to_user(html_path,bcc_users,row)

def send_html_weblink_by_csv(html_path, bcc_users, csv_path,weblink):
    with open(csv_path, 'r') as file:
        reader = csv.reader(file)
        next(reader)
        for row in reader:
            send_email_to_user(html_path,bcc_users,row,weblink)


if __name__ == "__main__":
    csv_path = "/home/gorla/Downloads/WelcomeCSV/WelcomeMailer_NandanGroup.csv"
    bcc_users = ("siddhartha.tomar@peoplestrong.com")
    #send_html_by_csv('./templates/welcome-zippi-mailer.html', bcc_users, csv_path)
    weblink ='https://nandangroup.zippi.co/'
    send_html_weblink_by_csv('./templates/welcome-zippi-mailer_weblink.html', bcc_users, csv_path,weblink)
