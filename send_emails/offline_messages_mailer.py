"""Connection to TalentPact and Qtrain Database"""
import pymssql
import sys
import time
import traceback
from configs import config as config
import mysql.connector
import requests
import redis
from datetime import datetime,timedelta
redis_conn = redis.Redis(
    host='localhost',
    port=6379,
    password='')

ENABLED_USER_LIST=["2721489","2205395","3593663"]
REATTEMPT_COUNT = 10
#SELECT_QUERY = "select a.userid ,b.bare_peer,count(a.msg_id), u.timezone from ejabberd.archive_data a" \
#               " join (select msg_id,bare_peer,timestamp,sender_id from ejabberd.archive_xml where " \
#               "kind=%s and organizationID=%s  and isEvent=false  and" \
#               " timestamp > UNIX_TIMESTAMP(NOW()- INTERVAL 5 HOUR)*1000000 and sender_id!='-501' " \
#               "and isDelForEveryone=false and timestamp < " \
#               "SOME(select UNIX_TIMESTAMP(NOW()- INTERVAL 4 HOUR)*1000000 as timestamp )) b " \
#               "on a.msg_id=b.msg_id and a.isActive=true and userid<>0 and b.sender_id<>a.userid" \
#               " join ejabberd.contacts  d on a.userid=d.userid and b.bare_peer<>concat(d.userid,'@',d.host)" \
#               " left outer join ejabberd.archive_status c on a.userid=c.userid and  a.msg_id=c.msg_id" \
#               " left outer join ejabberd.jabberIdUserIdMapping u on u.userId=a.userid where " \
#               "(c.userid is null or c.isRead=1 )and  a.mail_sent = false and  " \
#               "a.timestamp > UNIX_TIMESTAMP(NOW()- INTERVAL 5 HOUR)*1000000 and u.timezone=%s group by a.userid, " \
#               " b.bare_peer"

SELECT_QUERY = "select a.userid ,b.bare_peer,count(a.msg_id), u.timezone, min(b.timestamp) as mintime from " \
               "ejabberd.archive_data a join (select msg_id,bare_peer,timestamp,sender_id from ejabberd.archive_xml" \
               " where kind=%s and organizationID=%s  and isEvent=false  and " \
               "timestamp > UNIX_TIMESTAMP(NOW()- INTERVAL %s HOUR)*1000000 and sender_id!='-501' and " \
               "isDelForEveryone=false) b on a.msg_id=b.msg_id and a.isActive=true and userid<>0 and" \
               " b.sender_id<>a.userid join ejabberd.contacts  d on a.userid=d.userid and " \
               "b.bare_peer<>concat(d.userid,'@',d.host) left outer join ejabberd.archive_status c" \
               " on a.userid=c.userid and  a.msg_id=c.msg_id left outer join ejabberd.jabberIdUserIdMapping u" \
               " on u.userId=a.userid where (c.userid is null or c.isRead=1 ) and u.timezone=%s  and a.mail_sent = false" \
               " group by a.userid,  b.bare_peer  having (mintime< UNIX_TIMESTAMP(NOW()- INTERVAL %s HOUR)*1000000)"



def connect_to_mysql_server():
    """
    Connect to MS-SQL server
    :return: Connection object
    """
    config.MYSQL_CONNECTION_EJABBERD = {
    "host": "10.226.0.200",
    "user": "ZippiServices",
    "passwd": "Zipp!$073s",
    "database": "ejabberd"}
    start = time.time()
    attempts = 1
    connected_boolean = False
    crsr = None
    MYSQL_CONNECTION_EJABBERD = config.MYSQL_CONNECTION_EJABBERD
    sql_connection_client = None

    while attempts <= REATTEMPT_COUNT and connected_boolean is False:
        try:
            sql_connection_client = mysql.connector.connect(host = MYSQL_CONNECTION_EJABBERD["host"],
                                                            user = MYSQL_CONNECTION_EJABBERD["user"],
                                                             passwd = MYSQL_CONNECTION_EJABBERD["passwd"],
                                                              database = MYSQL_CONNECTION_EJABBERD["database"])
            #crsr = sql_connection_client
            connected_boolean = True
            print("Connection Successful")
        except Exception:
            print(traceback.format_exc())
            attempts += 1
            print("Reconnect attempt : {0}".format(attempts))
            time.sleep(5)
    if connected_boolean is False:
        print("Failed connecting to SQL Server. Check Error Log")
    end = time.time()
    print("SQL-CONNECTOR", "Took: " + str(end - start) + " seconds.")
    return sql_connection_client

def send_one_mailer(user_id,host, senders_jid_list_str):
    if str(user_id) in ENABLED_USER_LIST:
        URL = "https://messenger.peoplestrong.com/XmppJinie/sendMailsToRecipients"
        senders_jid_list =senders_jid_list_str.split(",")
        user_jid = user_id+"@"+host
        payload =   {"messageType": "chat",
                    "from": senders_jid_list,
                    "senderName": "",
                    "chatType": "0",
                    "to": user_jid,
                    "isinstalled": True
                }
        headers = {"Content-Type": "application/json",
                   "Authorization": "JinieNotificationITGGN575"}
        res = requests.post(url=URL, json=payload, headers=headers)
        print (payload)
        print(res.text)
    else:
        print("Not Enabled user")

def send_group_mailer(user_id,host,senders_list):
    if str(user_id) in ENABLED_USER_LIST:
        URL = "https://messenger.peoplestrong.com/XmppJinie/sendMailsToRecipients"
        print("send Group")
        user_jid = user_id + "@" + host
        payload ={
                "messageType": "chat",
                "fromGroup": senders_list,
                "senderName": "",
                "chatType": "1",
                "to": user_jid,
                "isinstalled": True
                }
        headers = {"Content-Type": "application/json",
                   "Authorization": "JinieNotificationITGGN575"}
        print(payload)
        res = requests.post(url=URL, json=payload, headers=headers)
        print(res.text)
    else:
        print("Not Enabled user")
def get_offline_message_user(org_id, timezone, message_type="chat"):
    try:
        max_threashold = 3
        min_threashhold= 2
        mysql_conn = connect_to_mysql_server()
        redis_key = "offline_msg_users_chat_" + message_type
        stored_users = redis_conn.hgetall(redis_key)
        previous_users = []
        print(stored_users)
        for redis_user, expiry_time in stored_users.items():
            if expiry_time < str(datetime.now()):
                redis_conn.hdel(redis_key, redis_user[0])
            else:
                previous_users.append(redis_user)
        cursor = mysql_conn.cursor()
        print(SELECT_QUERY)
        print("TimeZone"+timezone)
        print("OrgId" + str(org_id))
        print("Datetime" + str(datetime.now()))
        cursor.execute(SELECT_QUERY, (message_type, org_id, max_threashold, timezone,min_threashhold))
        details = cursor.fetchall()
        cursor.close()
        final_user_data_dict= dict()
        active_user = []
        print("Previous Users")
        print(previous_users)
        check_previous_que = previous_users
        for user_info in details:
            if user_info[0] not in check_previous_que:
                active_user.append(user_info)
            #else:
                #check_previous_que.remove(user_info[0])

        for user_data in active_user:
            #if user_data[0] in ENABLED_USER_LIST:
            if message_type == 'chat':
                get_sender_data = final_user_data_dict.get(user_data[0], None)
                if get_sender_data is None:
                    final_user_data_dict[user_data[0]] = user_data[1]
                else:
                    final_user_data_dict[user_data[0]] = get_sender_data + ", " + user_data[1]

            elif message_type == 'groupchat':
                get_sender_data = final_user_data_dict.get(user_data[0], None)
                if get_sender_data is None:
                    final_user_data_dict[user_data[0]] = [{"groupId": user_data[1],"count":str(user_data[2])}]
                else:
                    senders_list = get_sender_data  +[{"groupId": user_data[1], "count": str(user_data[2])}]
                    final_user_data_dict[user_data[0]] = senders_list
        #print(final_user_data_dict)
        for user_key, sender_data in final_user_data_dict.items():
            #print sender_data
            #print user_key
            if (user_key in ENABLED_USER_LIST ) and (str(user_key) not in previous_users):
                print(user_key)
                print(previous_users)
                expiry_time = datetime.now() + timedelta(hours=min_threashhold)
                redis_conn.hdel(redis_key, user_key)
                redis_conn.hset(redis_key, user_key, str(expiry_time))
                if message_type == 'chat':
                    sender_str = sender_data
                    host = sender_str.split(",")[0].split("@")[1]
                    send_one_mailer(user_key,host,sender_str)
                elif message_type == 'groupchat':
                    sender_list = sender_data
                    host = sender_list[0].get("groupId", "").split("@conference.")[1]
                    send_group_mailer(user_key, host, sender_list)

    except Exception as e:
        print("Error while runnig script", e)
    finally:
        if (mysql_conn.is_connected()):
            mysql_conn.close()
            print("MySQL connection is closed")


if __name__ == "__main__":
    args_lenth = len(sys.argv)
    print(str(sys.argv))
    if args_lenth == 3:
        timezone = sys.argv[1]
        msg_type = sys.argv[2]
        print("cron is runnig for timezone "+str(timezone))
        print("cron is runnig for time " + str(datetime.now()))
        print("cron is runnig for msg_typ " + str(msg_type))
        get_offline_message_user(19, timezone, msg_type)
    else:
        print("Please enter timezone, message type as commandline parameters")