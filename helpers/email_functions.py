import smtplib
import traceback

EMAIL_TEMPLATE = """From: %s <%s>
To: %s
MIME-Version: 1.0
Content-type: text/html
Subject: %s

%s
"""


def send_mail(sender_email, friendly_sender_name, messages, options=None):
    smtp_server = None
    response = {'status': False, 'message': ''}
    if not sender_email and not isinstance(sender_email, basestring):
        response['message'] = "Parameter sender_email should be string or unicode."
    elif not friendly_sender_name and not isinstance(friendly_sender_name, basestring):
        response['message'] = "Parameter friendly_sender_name should be string or unicode."
    elif not isinstance(messages, list):
        response['message'] = "Parameter messages should be a list."
    elif sender_email == "altml@alt.peoplestrong.com":
        try:
            smtp_server = smtplib.SMTP(host="email-1.peoplestrong.com", port=587)
            smtp_server.starttls()
            smtp_server.login(sender_email, "AltData@456")
            results = []
            success = True
            for message in messages:
                smtp_message = EMAIL_TEMPLATE % (
                friendly_sender_name, sender_email, message['recipient_email'], message['subject'], message['body'])
                response = smtp_server.sendmail(from_addr=sender_email, to_addrs=message['recipient_email'],
                                                msg=smtp_message)
                # response is empty if everything went well
                if response:
                    success = False
                results.append(response)

            if success:
                response = {'status': True, 'message': ''}
            else:
                response['message'] = str(results)

        except Exception:
            response['message'] = traceback.format_exc()
    else:
        response['message'] = "Sender email is incorrect. You should know the sender email to use this function!"

    try:
        if smtp_server:
            smtp_server.quit()
    except Exception:
        pass

    return response


def _test_send_mail():
    print send_mail(sender_email="altml@alt.peoplestrong.com",
                    friendly_sender_name="Amoeba",
                    messages=[{
                        "recipient_email": "aakash.roy@peoplestrong.com",
                        "subject": "Testing email from Python",
                        "body": "Hello"
                    }
                        # , {
                        #     "recipient_email": "sumit.chhabra@peoplestrong.com",
                        #     "subject": "Testing email from Python",
                        #     "body": "Hello"
                        # },
                        #     {
                        #         "recipient_email": "hartaran.singh@peoplestrong.com",
                        #         "subject": "Testing email from Python",
                        #         "body": "Hello"
                        #     }
                    ])


if __name__ == "__main__":
    _test_send_mail()
