#!/bin/sh

csv_file="welcome_mail.csv"
csv_folder="/opt/python-projx/WelcomeCSV/"
csv_path=$csv_folder$csv_file 
export PYTHONPATH=/opt/python-projx/jinienotificationmangementtool
cd /opt/python-projx/jinienotificationmangementtool

echo $csv_path
/opt/python-projx/jinienotificationmangementtool/venv/bin/python /opt/python-projx/jinienotificationmangementtool/send_emails/send_email_by_shell.py "$csv_path" > /var/log/welcome_mailer_report.txt

/opt/python-projx/jinienotificationmangementtool/venv/bin/python /opt/python-projx/jinienotificationmangementtool/send_emails/send_mail_logger.py "/var/log/welcome_mailer_report.txt"
