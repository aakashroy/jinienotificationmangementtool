from py2neo import Graph, Node, Relationship

from configs import config
from helpers.db_helpers import connect_to_sql_server

graph = Graph(host="192.168.2.210", password="neo4j@123")
print graph
print graph.delete_all()

QUERY = "select hre.EmployeeID, hre.OfficialEmailAddress, hre.UserId, su.FirstName, su.MiddleName, su.LastName, \
         hre.EmployeeCode, hre.HrManagerID, hre.L1ManagerID, hre.L2ManagerID \
         from dbo.HrEmployee as hre \
         inner join dbo.SysUser as su on su.UserID=hre.UserID \
         where hre.OrganizationId=%s and hre.EmploymentStatusID in %s \
         and hre.OfficialEmailAddress is not NULL"

sql_con = connect_to_sql_server(config)
sql_con.execute(QUERY, (19, [210, 214]))
users_to_sync = sql_con.fetchall()
users_in_sql_list = [(empid, eml, usrid, fn, mn, ln, ec, hmid, l1mid, l2mid)
                     for (empid, eml, usrid, fn, mn, ln, ec, hmid, l1mid, l2mid) in users_to_sync]
org_users = dict()
org_relationships = list()
for user in users_in_sql_list:
    user_node = Node("Employee", emp_id=user[0], official_email=user[1], user_id=user[2],
                     first_name=user[3], middle_name=user[4], last_name=user[5], emp_code=user[6],
                     hr_mgr_id=user[7], l1_mgr_id=user[8], l2_mgr_id=user[9], org_id=19)
    org_users[user[0]] = user_node

tx = graph.begin()

for user in users_in_sql_list:
    l1 = user[8]
    l2 = user[9]
    hr = user[7]

    if l1:
        org_relationships.append(Relationship(org_users[user[0]], "L1_MANAGED_BY", org_users[l1]))
        org_relationships.append(Relationship(org_users[l1], "L1_MANAGES", org_users[user[0]]))
    if l2:
        org_relationships.append(Relationship(org_users[user[0]], "L2_MANAGED_BY", org_users[l2]))
        org_relationships.append(Relationship(org_users[l2], "L2_MANAGES", org_users[user[0]]))
    if hr:
        org_relationships.append(Relationship(org_users[user[0]], "HR_MANAGED_BY", org_users[hr]))
        org_relationships.append(Relationship(org_users[hr], "HR_MANAGES", org_users[user[0]]))

tot = len(org_users.items())
i = 0
for emp_id, user in org_users.items():
    if i % 100 == 0:
        print("Creating {}/{}".format(i, tot))
    tx.create(user)
    i = i + 1

tot = len(org_relationships)
i = 0
for relationship in org_relationships:
    if i % 100 == 0:
        print("Creating {}/{}".format(i, tot))
    tx.create(relationship)
    i = i + 1

tx.commit()

print("Done")
